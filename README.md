# clap-test

This is a trivial test / example of the rust clap crate for command line argument parsing.

I couldn't find any examples in the clap code itself which showed how to get it to generate
the completion scripts for the various different shells it supports, and the single online
example I did find was a couple of years out of date.