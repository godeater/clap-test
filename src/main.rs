#[macro_use]
extern crate clap;
extern crate yaml_rust;

use std::io;
use clap::Shell;

mod cli;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = cli::build_cli_from_yaml(yaml).get_matches();

    match matches.subcommand() {
        ("completions", Some(sub_matches)) => {
            let shell = sub_matches.value_of("shell").unwrap();
            match shell {
                "bash" => {
                    cli::build_cli_from_yaml(yaml).gen_completions_to("clap-test", Shell::Bash, &mut io::stdout());
                },
                "fish" => {
                    cli::build_cli_from_yaml(yaml).gen_completions_to("clap-test", Shell::Fish, &mut io::stdout());
                },
                "powershell" => {
                    cli::build_cli_from_yaml(yaml).gen_completions_to("clap-test", Shell::PowerShell, &mut io::stdout());
                },
                "zsh" => {
                    cli::build_cli_from_yaml(yaml).gen_completions_to("clap-test", Shell::Zsh, &mut io::stdout());
                },
                _ => unimplemented!(),
            }
        },
        (_, _) => unimplemented!(),
    }
}