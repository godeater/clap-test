use clap::App;
use yaml_rust::yaml::Yaml;

pub fn build_cli_from_yaml<'a>(yaml: &'a Yaml) -> App<'a, 'a> {
    App::from_yaml(yaml)
}